#include<iostream>
#include <vector>
#include <list>
#include "graph.h"

using namespace std;

void main() {

	int metrix[4][4] = { {0,5,2,1},{5,0,4,1},{2,1,0,1},{1,1,1,0} };
	Graph myGraph;

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << metrix[i][j] << " ";
		}
		cout << endl;
	}

	myGraph.insert(metrix);
	myGraph.printGraph();

	if (myGraph.CompleteGraph())
	{
		cout << "This graph is complete" << endl;
	}
	else
	{
		cout << "This graph is not complete" << endl;
	}

	if (myGraph.WeightedGraph())
	{
		cout << "This graph is weighted" << endl;
	}
	else
	{
		cout << "This graph is not weighted" << endl;
	}

	if (myGraph.digraph())
	{
		cout << "This graph is directed graph" << endl;
	}
	else
	{
		cout << "This graph is not directed graph" << endl;
	}

	if (myGraph.pseudograph())
	{
		cout << "This graph is pseudo graph" << endl;
	}
	else
	{
		cout << "This graph is not pseudo graph" << endl;
	}
	if (myGraph.multigraph())
	{
		cout << "This graph is multi graph" << endl;
	}
	else
	{
		cout << "This graph is not multi graph" << endl;
	}

	myGraph.dykstra('A');

	system("pause");

}